import axios from 'axios';

export function fetchPosts() {
    return axios.get('http://localhost:8080/posts');
}

export function addPost(body) {
    return axios.post('http://localhost:8080/posts', body);
} 