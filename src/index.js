import { form } from "./dom";
import { handleFormSubmit, populatePosts } from "./handlers";

populatePosts();

form.addEventListener("submit", handleFormSubmit);
