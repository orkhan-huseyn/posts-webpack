import { addPost, fetchPosts } from "./api";
import { postList } from "./dom";
import { timeAgo } from "./timeAgo";

export async function handleFormSubmit(event) {
  event.preventDefault();
  const { title, content } = this.elements;

  const requestBody = {
    title: title.value,
    content: content.value,
    created_at: new Date().toISOString(),
  };

  await addPost(requestBody);

  title.value = "";
  content.value = "";

  populatePosts();
}

export async function populatePosts() {
  const { data: posts } = await fetchPosts();

  let html = "";
  for (let post of posts) {
    html += `
    <div class="list-group-item list-group-item-action">
      <div class="d-flex w-100 justify-content-between">
        <h5 class="mb-1">${post.title}</h5>
        <small>${timeAgo.format(new Date(post.created_at))}</small>
      </div>
      <p class="mb-1">${post.content}</p>
    </div>
    `;
  }

  postList.innerHTML = html;
}
